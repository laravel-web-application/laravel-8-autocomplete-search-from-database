<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;

class AutocompleteSearchDBController extends Controller
{
    public function index()
    {
        return view('autocomplete-textbox-search');
    }

    public function searchDB(Request $request)
    {
        $search = $request->get('term');

        $result = User::where('name', 'LIKE', '%' . $search . '%')->get();

        return response()->json($result);

    }
}
