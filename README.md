<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400"></a></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/d/total.svg" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/v/stable.svg" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/license.svg" alt="License"></a>
</p>

## Laravel Autocomplete
### Things to do list:
1. Clone this repository: `git clone https://gitlab.com/laravel-web-application/laravel-8-autocomplete-search-from-database.git`
2. Go inside the folder: `cd laravel-8-autocomplete-search-from-database`
3. Run `cp .env.example .env` then put your database name, credentials & AWS Key.
4. Run `composer install`
5. Run `php artisan key:generate`
6. Run `php artisan migrate --seed`
7. Run `php artisan serve`
8. Open your favorite browser: http://localhost:8000/autocomplete-textbox

### Screen shot
Autocomplete Textbox

![Autocomplete Textbox](img/search.png "Autocomplete Textbox")


